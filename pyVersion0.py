# global grid
# global cages
# https://www.dailykillersudoku.com/puzzle/21120


class cage:
    def __init__(self, total, cells):
        self.total = total
        self.cells = cells
        self.possibilities = self.findCombinations()

    def findCombinations(self):
        results = []
        self.sumOptions(len(self.cells), self.total, 0, 1, [], results)
        return results

    def sumOptions(self, K, N, sum, start, combinations, results):
        if K == 0:
            if sum == N:
                results.append([] + combinations)
            return

        for i in range(start, 10):
            combinations.append(i)
            self.sumOptions(K-1, N, sum + i, i + 1, combinations, results)
            combinations.pop(-1)
        return

    def removePoss(self, poss):
        """Removes options from a cage's possibilities.
        """
        for i in poss:
            if self.possibilities.count(i) > 0:
                self.possibilities.remove(i)

    def updatePoss(self):
        remove = []
        # remove possibilities based on values in a cage
        for cell in self.cells:
            val = getCell(cell).value
            if val != 0:
                for poss in self.possibilities:
                    if poss.count(val) == 0:
                        remove.append(poss)

        # Remove cage possibilities based on cell possibilities
        for poss in self.possibilities:
            for i in poss:
                present = False
                for cell in self.cells:
                    cellPoss = getCell(cell).possibilities
                    if cellPoss.count(i) > 0:
                        present = True
                if not(present):
                    remove.append(poss)
                    break
        self.removePoss(remove)
        # Check if value must appear somewhere in cage
        if len(self.possibilities) == 1:
            print(self.total, " has 1 possibility left")
            cellPoss = []
            for i in self.cells:
                cellPoss += getCell(i).possibilities
            print(self.possibilities, cellPoss)
            for i in self.possibilities[0]:
                if cellPoss.count(i) == 1:
                    for cell in self.cells:
                        if getCell(cell).possibilities.count(i) > 0:
                            getCell(cell).setValue(i)


class cell:
    def __init__(self, value, x, y):
        self.value = value
        self.possibilities = [i for i in range(1, 10)]
        self.x = x
        self.y = y

    def setCage(self, cage):
        self.cage = cage

    def setValue(self, value):
        """Set's the cells value 'correctly'
        """
        self.value = value
        self.possibilities = [value]

    def removePoss(self, poss):
        """Removes a list of values from a cells list of possibilities
        """
        if self.value != 0:  # No point updating possibilities of solved cells
            return
        for i in poss:
            if self.possibilities.count(i) > 0:
                self.possibilities.remove(i)
        if len(self.possibilities) < 2:
            print("only one option left for ", self.x, ",", self.y)
            self.setValue(self.possibilities[0])

    def squareCheck(self):
        """Checks for values in the same square as the cell
        """
        remove = []
        squareStart = getSquareStart((self.x, self.y))
        for rowIndex in range(squareStart[0], squareStart[0]+3):
            for columnIndex in range(squareStart[1], squareStart[1]+3):
                val = getCell((rowIndex, columnIndex)).value
                if val != 0:
                    remove.append(val)
        return remove

    def columnCheck(self):
        """Checks for values in the same column as the cell.
        """
        remove = []
        for rowIndex in range(0, 9):
            val = getCell((self.x, rowIndex)).value
            if val != 0:
                remove.append(val)
        return remove

    def rowCheck(self):
        """Checks for values in the same row as the cell
        """
        remove = []
        for cell in grid[self.y-1]:
            if cell.value != 0:
                remove.append(cell.value)
        return remove

    def updatePoss(self):
        """ Attempts to solve the cell
        """
        # Reducing cells possibilities based on cage possibilities
        remove = []
        for i in [] + self.possibilities:
            present = False
            for a in self.cage.possibilities:
                if a.count(i) > 0:
                    present = True
                    break
            if not(present):
                remove.append(i)

        # Reducing cell possibilities base on other cage values
        for cell in self.cage.cells:
            val = getCell(cell).value
            if val != 0:
                remove.append(val)

        remove += self.rowCheck()
        remove += self.columnCheck()
        remove += self.squareCheck()

        self.removePoss(remove)


def prettyPrint():
    """Prints out the grid neatly
    """
    for a in grid:
        print([b.value for b in a])


def setUpCages():
    """Assigns each cells the correct cage
    """
    for currentCage in cages:
        for cellPos in currentCage.cells:
            grid[cellPos[1] - 1][cellPos[0] - 1].setCage(currentCage)


def getCell(coords):
    """Returns the cell at a given x, y takes a tuple
    """
    return grid[coords[1]-1][coords[0]-1]


def getSquareStart(coords):
    """Returns the the coordinates of top left of the square a given cells in.
    """
    return (coords[0] - ((coords[0]-1) % 3), coords[1] - ((coords[1]-1) % 3))


def squareCheck(squareStart):
    """Checks if a value must appear in a postition in a square
    """
    squarePoss = []
    for i in range(0, 3):
        for ii in range(0, 3):
            squarePoss.append(getCell(squareStart+(i, ii)).possibilities)
    # TODO: Finish this function



# Grid initialization

grid = [[cell(0, i+1, ii+1) for i in range(0, 9)] for ii in range(0, 9)]
cages = [
            cage(15, [(1, 1), (1, 2), (1, 3)]),
            cage(7, [(5, 1), (6, 1), (7, 1)]),  # The special one
            cage(5, [(2, 1), (2, 2)]),
            cage(12, [(3, 1), (3, 2)]),
            cage(9, [(4, 1), (4, 2)]),
            cage(7, [(5, 1), (6, 1), (7, 1)]),
            cage(18, [(8, 1), (9, 1), (9, 2)]),
            cage(22, [(2, 3), (3, 3), (4, 3)]),
            cage(8, [(5, 2), (6, 2)]),
            cage(11, [(7, 2), (7, 3)]),
            cage(15, [(8, 2), (8, 3), (9, 3)]),
            cage(19, [(5, 3), (6, 3), (6, 4)]),
            cage(9, [(1, 4), (2, 4)]),
            cage(12, [(3, 4), (3, 5)]),
            cage(10, [(4, 4), (4, 5)]),
            cage(19, [(5, 4), (5, 5), (5, 6)]),
            cage(17, [(7, 4), (8, 4)]),
            cage(7, [(9, 4), (9, 5), (8, 5)]),
            cage(21, [(1, 5), (1, 6), (2, 5)]),
            cage(5, [(6, 5), (6, 6)]),
            cage(11, [(7, 5), (7, 6)]),
            cage(3, [(2, 6), (3, 6)]),
            cage(18, [(4, 6), (4, 7), (5, 7)]),
            cage(10, [(8, 6), (9, 6)]),
            cage(20, [(1, 7), (2, 7), (2, 8)]),
            cage(6, [(3, 7), (3, 8)]),
            cage(14, [(6, 7), (7, 7), (8, 7)]),
            cage(19, [(9, 7), (9, 8), (9, 9)]),
            cage(12, [(1, 8), (1, 9), (2, 9)]),
            cage(6, [(4, 8), (5, 8)]),
            cage(11, [(6, 8), (6, 9)]),
            cage(10, [(7, 8), (7, 9)]),
            cage(11, [(8, 8), (8, 9)]),
            cage(13, [(3, 9), (4, 9), (5, 9)])
            ]  # Stores all the diffrent cages
setUpCages()

# Testing

grid[0][4].setValue(4)
grid[0][5].setValue(2)
grid[0][6].setValue(1)
getCell((9, 4)).setValue(1)
prettyPrint()

for a in range(0, 3):
    for cage in cages:
        cage.updatePoss()
    for i in range(0, 9):
        for ii in range(0, 9):
            grid[i][ii].updatePoss()

print(getCell((4, 9)).cage.possibilities)
prettyPrint()
